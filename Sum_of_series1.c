//program to find the sum of series
//   1/1!+4/4!+9/3!+...



#include<stdio.h>
int fact(int);
int main()
{
	int n,i,num,deno;
	float sum=0.0;
	printf("enter the value of n\n");
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		num=pow(i,i);
		deno=fact(i);
		sum+=(float)num/deno;
	}
	printf("sum=%.2f",sum);
	return 0;
}
int fact(int n)
{
	int f=1,i;
	for(i=n;i>=1;i--)
	f=f*i;
	return f;
}