//program to find sum and avg of first 'n' no.'s using do while loop
#include<stdio.h>
#include<math.h>
int main()
{
	int n,i=1,sum=0;
	float avg=0.0;
	printf("enter the no. of numbers upto which sum & avg is to be found\n");
	scanf("%d",&n);
	do
	{
		sum=sum+i;
		i=i+1;
	}while(i<=n);
	avg=(float)(sum)/n;
	printf("sum=%d",sum);
	printf("avg=%f",avg);
	return 0;
}