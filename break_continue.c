//program to read numbers from 1 to 100 and skip 
//numbers divisible by 3
//and add their sum
//print sum
#include<stdio.h>
int main()
{
	int i,sum;	
	for(sum=0,i=1;i<=100;i++)
	{
		if(i%3==0)continue;
		sum+=i;
	}
	printf("sum=%d",sum);
	return 0;
}
