//program to find smallest and largest of array and print their location & also swap the largest and smallest numbers and then print the 
//array after swapping those numbers


#include<stdio.h>
int main()
{
	int a[20],i,n,big,small,locbig,locsmall,temp;	
	printf("enter the size of an array\n");
	scanf("%d",&n);
	printf("enter the elements of array\n");
	for(i=0;i<n;i++)
	{

		scanf("%d",&a[i]);
	}
	small=a[0];
	big=a[0];
	for(i=0;i<n;i++)
	{
		if(a[i]>=big)
		{
			big=a[i];
			locbig=i;
		}
		if(a[i]<=small)
		{
			small=a[i];
			locsmall=i;
		}
	}

	printf("big=%d and locbig=%d",big,locbig);
	printf("small=%d and locsmall=%d",small,locsmall);
	temp=a[locbig];
	a[locbig]=a[locsmall];
	a[locsmall]=temp;
	printf("array after swapping is \n");
	for(i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
	return 0;
}
